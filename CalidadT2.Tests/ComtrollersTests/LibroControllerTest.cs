﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Tests.ComtrollersTests
{
    class LibroControllerTest
    {
        [Test]
        public void detailsIsnotNull()
        {
            var mock = new Mock<ILibroService>();
            mock.Setup(a => a.libroDetalles(8))
                .Returns(new Libro
                {
                    Id = 8,
                    Nombre = "Harry Potter 2",
                    Autor = new Autor { Id = 5, Nombres = "JkRowling" }

                });
            var controller = new LibroController(mock.Object, null, null);
            var view = controller.Details(8) as ViewResult;
            Assert.IsNotNull(view);
            Assert.IsInstanceOf<Libro>(view.Model);
        }

        [Test]
        public void AddComentarioIsnotNull()
        {
            Comentario comentario = new Comentario
            {
                LibroId = 8,
                Puntaje = 2,
                Texto = "Estubo muy buena recomendado papu ",
                Id = 8
            };
            var mockLibro = new Mock<ILibroService>();
            mockLibro.Setup(a => a.buscarLibro(8)).Returns(new Libro { Puntaje = 4 });
            mockLibro.Setup(a => a.saveChanges());
            var mockComents = new Mock<IComentarioService>();
            mockComents.Setup(a => a.agregarComentario(comentario));

            var controller = new LibroController(mockLibro.Object, mockComents.Object, null);
            var view = controller.AddComentario(comentario) as RedirectToRouteResult;
            Assert.IsInstanceOf<RedirectToRouteResult>(view);
            Assert.IsNotNull(view);
        }
    }
}
