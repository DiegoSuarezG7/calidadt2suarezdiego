﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Tests.ComtrollersTests
{
    class HomeControllerTest
    {
        [Test]
        public void testIsnotNulModel()
        {
            var mock = new Mock<ILibroService>();
            mock.Setup(a => a.LibrosIncludeAutor()).Returns(new List<Libro>());
            var controller = new HomeController(mock.Object);
            var view = controller.Index() as ViewResult;
            Assert.IsNotNull(view);
            Assert.IsInstanceOf<List<Libro>>(view.Model);
        }
    }
}
