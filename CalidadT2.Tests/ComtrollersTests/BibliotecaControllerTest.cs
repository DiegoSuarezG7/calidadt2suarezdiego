﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Tests.ComtrollersTests
{
    class BibliotecaControllerTest
    {
        [Test]
        public void testIndexnotNull()
        {
            var mock = new Mock<IBibliotecaService>();
            mock.Setup(a => a.LibrosBiblioteca(1)).Returns(new List<Biblioteca>() {
                new Biblioteca (),new Biblioteca (),
            });
            var controller = new BibliotecaController(mock.Object,null);
            var view = controller.Index() as ViewResult;
            var modelo = (List<Biblioteca>)view.Model;
            Assert.AreEqual(modelo.Count, 2);
            Assert.IsInstanceOf<List<Biblioteca>>(view.Model);
            Assert.IsNotNull(view);
        }

        [Test]
        public void addnotNull()
        {
            var mock = new Mock<IBibliotecaService>();
            mock.Setup(a => a.BibliotecaAdd(new Biblioteca()));
            var mockusuario = new Mock<IUsuarioService>();
            var controller = new BibliotecaController(mock.Object, mockusuario.Object);
            var view = controller.Add(4) as RedirectToRouteResult;
            var valor = controller.TempData;
            Assert.IsNotNull(valor["SuccessMessage"]);
            Assert.IsInstanceOf<RedirectToRouteResult>(view);
        }

        [Test]
        public void MarcarComoLeyendoNull()
        {
            var mock = new Mock<IBibliotecaService>();
            mock.Setup(a => a.LibroyUsuarioDeBiblioteca(1, 1)).Returns(new Biblioteca());
            mock.Setup(a => a.SaveChanges());

            var controller = new BibliotecaController(mock.Object,null);
            var view = controller.MarcarComoLeyendo(1) as RedirectToRouteResult;
            var valor = controller.TempData;
            Assert.IsNotNull(valor["SuccessMessage"]);
            Assert.IsInstanceOf<RedirectToRouteResult>(view);

        }

        [Test]
        public void MarcarComoTerminadoNull()
        {
            var mock = new Mock<IBibliotecaService>();
            mock.Setup(a => a.LibroyUsuarioDeBiblioteca(1, 1)).Returns(new Biblioteca());
            mock.Setup(a => a.SaveChanges());

            var controller = new BibliotecaController(mock.Object,null);
            var view = controller.MarcarComoTerminado(1) as RedirectToRouteResult;
            var valor = controller.TempData;
            Assert.IsNotNull(valor["SuccessMessage"]);
            Assert.IsInstanceOf<RedirectToRouteResult>(view);

        }
    }
}
