﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2.Tests.ComtrollersTests
{
    class AuthControllerTest
    {
        [Test]
        public void LoginISnotnull()
        {
            var userMock = new Mock<IUsuarioService>();
            userMock.Setup(o => o.GetUser("admin", "admin")).Returns(new Usuario { });

            var authMock = new Mock<IAuthCookieService>();

            var controller = new AuthController(userMock.Object, authMock.Object);
            var result = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }

        [Test]
        public void logoutIsNull()
        {
            Usuario us = null;

            var mock = new Mock<IUsuarioService>();
            mock.Setup(a => a.GetUser("admin", "admin")).Returns(us);
            var controller = new AuthController(mock.Object, null);
            var view = (ViewResult)controller.Login("admin", "admin");

            Assert.IsNull(view.Model);
            
        }
    }
}
