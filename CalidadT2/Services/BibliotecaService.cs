﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public interface IBibliotecaService
    {
        List<Biblioteca> LibrosBiblioteca(int userid);
        void BibliotecaAdd(Biblioteca biblioteca);
        Biblioteca LibroyUsuarioDeBiblioteca(int libroId, int userid);
        void SaveChanges();
    }

    public class BibliotecaService : IBibliotecaService
    {
        private readonly AppBibliotecaContext app;

        public BibliotecaService(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public List<Biblioteca> LibrosBiblioteca(int userid)
        {
            return app.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == userid)
                .ToList();

        }

        public void BibliotecaAdd(Biblioteca biblioteca)
        {

            app.Bibliotecas.Add(biblioteca);
            app.SaveChanges();
        }

        public Biblioteca LibroyUsuarioDeBiblioteca(int libroId, int userid)
        {
            return app.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == userid)
                .FirstOrDefault();
        }

        public void SaveChanges()
        {
            app.SaveChanges();
        }
    }
}
