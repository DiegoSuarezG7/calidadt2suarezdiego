﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public interface IComentarioService
    {
        void agregarComentario(Comentario comentario);
    }
    public class ComentarioService : IComentarioService
    {
        private readonly AppBibliotecaContext app;

        public ComentarioService(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public void agregarComentario(Comentario comentario)
        {
            app.Comentarios.Add(comentario);
        }
    }
}


