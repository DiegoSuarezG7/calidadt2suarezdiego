﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public interface ILibroService
    {
        Libro libroDetalles(int id);
        Libro buscarLibro(int comentarioLibroId);
        void saveChanges();
        List<Libro> LibrosIncludeAutor();
    }
    public class LibroService : ILibroService
    {
        private readonly AppBibliotecaContext app;

        public LibroService(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public Libro libroDetalles(int id)
        {
            return app.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == id)
                .FirstOrDefault();
        }

        public Libro buscarLibro(int comentarioLibroId)
        {
            return app.Libros.Where(o => o.Id == comentarioLibroId).FirstOrDefault();
        }

        public void saveChanges()
        {
            app.SaveChanges();
        }

        public List<Libro> LibrosIncludeAutor()
        {
            return app.Libros.Include(o => o.Autor).ToList();
        }

    }
}
