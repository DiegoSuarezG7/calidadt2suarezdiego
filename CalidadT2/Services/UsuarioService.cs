﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public interface IUsuarioService
    {
        Usuario GetUser(string username, string password);
        Usuario getLogeado(Claim claim);
        Claim OptieneHttpContex();

    }
    public class UsuarioService : IUsuarioService
    {
        private readonly AppBibliotecaContext app;
        private HttpContext httpContext;

        public UsuarioService(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public Usuario GetUser(string username, string password)
        {
            var user = app.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
            return user;
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }
        public Claim OptieneHttpContex()
        {
            return httpContext.User.Claims.FirstOrDefault();
        }

        public Usuario getLogeado(Claim claim)
        {
            return app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
        }

    }
}
