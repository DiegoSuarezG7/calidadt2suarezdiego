﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Services
{
    public interface IAuthCookieService
    {
        void SetHttpContext(HttpContext httpContext);
        void Login(ClaimsPrincipal principal);
        //Usuario usuarioLogeado();
        Claim claimDeUsuario();
        Usuario LoggedUser();
    }
    public class AuthCookieService : IAuthCookieService
    {
        private HttpContext httpContext;
        private readonly AppBibliotecaContext app;
        public AuthCookieService(AppBibliotecaContext app)
        {
            this.app = app;
        }
        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }
        public void Login(ClaimsPrincipal principal)
        {
            httpContext.SignInAsync(principal);
        }

        //public Usuario usuarioLogeado()
        //{
        //    var claim = httpContext.User.Claims.FirstOrDefault();
        //    return app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            
        //}

        public Claim claimDeUsuario()
        {
            return httpContext.User.Claims.FirstOrDefault();
        }

        public Usuario LoggedUser()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
