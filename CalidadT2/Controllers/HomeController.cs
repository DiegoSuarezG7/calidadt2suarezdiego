﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Services;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILibroService libroService;
        public HomeController(ILibroService libroService)
        {
            this.libroService = libroService;
        }

        [HttpGet]
        public IActionResult Index()
        {            
            var model = libroService.LibrosIncludeAutor();
            return View(model);
        }
    }
}
