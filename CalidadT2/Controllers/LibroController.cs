﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroService libroService;
        private readonly IComentarioService comentarioService;
        private readonly IUsuarioService usuarioService;

        public LibroController(ILibroService libroService, IComentarioService comentarioService, IUsuarioService usuarioService)
        {
            this.libroService = libroService;
            this.comentarioService = comentarioService;
            this.usuarioService = usuarioService;
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            return View(libroService.libroDetalles(id));
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;

            comentarioService.agregarComentario(comentario);

            var libro = libroService.buscarLibro(comentario.LibroId);

            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            libroService.saveChanges();

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        //private Usuario LoggedUser()
        //{
        //    var claim = usuarioService.OptieneHttpContex();
        //    var user = usuarioService.getLogeado(claim);
        //    return user;

        //}
        private Usuario LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = usuarioService.getLogeado(claim);
            return user;
        }


    }
}
